### Tema pentru croco smecheri:
##### JS Vanilla:
1.Declarati un JSON numit cumparaturi cu proprietatile numeMagazin(string), listaCumparaturi vector(string), pretProduse vector(numeric)
Asignati valorile variabilelor de mai jos in JSON-ul creat si afisati in consola json-ul.
``tip: variabila produse de tip string trebuie convertita intr-un vector de string-uri``

**Output dorit:** 
```
{
  numeMagazin: 'Carrefour',
  listaCumparaturi: [ 'Tigari', 'Bere', 'Fructe', 'Apa', 'Cola' ],
  pretProduse: [ 21.5, 3, 6, 2.5, 3.5 ]
}
```
2.Meșteriți o functie ce primeste ca parametru json-ul creat la exercitiul 1 si returneaza un vector de string unde fiecare element este de forma produs-pret. Afisati vectorul rezultat la consola pentru a testa rezultatul.

**Output dorit:**
```
[ 'Tigari-21.5', 'Bere-3', 'Fructe-6', 'Apa-2.5', 'Cola-3.5' ]
```

![gifObosit](https://media.giphy.com/media/iVDo6InQKyW8o/giphy.gif)

``Merge? Bravo clan, trecem in etapa urmatoare!``

##### Node.js
1.Meșteriți o rută de tip GET ce va intoarce un crocodil al carui id este specificat in ruta
 ``` 
Exemplu apel in Postman: GET http://localhost:8080/getCroco/2 
```
`` tip: Introduceti in bd cativa crocos(3-4) ca sa aveti pe ce sa testati get-ul de mai sus. ``

2.Meșteriți și ultima ruta de tip PUT care va permite sa schimbati email-ul unui croco al carui id este specificat in ruta.

```
Exemplu apel in Postman: PUT http://localhost:8080/updateCroco/2
```
`` tip: Folositi-va de ruta de GET creata mai sus pentru a testa daca acest PUT a functionat corect ``

### Felicitari! Esti la un pas mai aproape de un back-end croco veritabil.
![gifObosit2](https://media.giphy.com/media/3oEhn4A5PQmhnYD3t6/giphy.gif)

***PS: Google is your friend***
***Pentru orice fel de intrebare suntem la dispozitia voastra la un mesaj distanta(Nu va fie teama sa intrebati indiferent de problema)***.

