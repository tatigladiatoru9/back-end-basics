//1. Afiseaza numele tau la consola
console.log('Alexandra + Madalin');

//2.Afiseaza la consola suma n numere
let sum = 0;
let n = 100;
sum = (n * (n + 1)) / 2;
console.log(sum);

//3. Afiseaza la consola suma n numere folosind o functie unde n este parametru
let sumaNr = function (n) {
    return (n * (n + 1)) / 2;
}

console.log(sumaNr(105));

// Arrow function
let sumaArrow = (n) => {
    return (n * (n + 1)) / 2;
}
console.log(sumaArrow(123));

//4.Moduri de parcurgere vectori
let vector = ["Cioroi", "Gainar", "Clan", "Croco", "SuntTare"];
let vectorNumeric = [2, 8, 20, 102, 4, 5, 89, 21, 45];

// basic for loop
for (let i = 0; i < vector.length; i++){
    console.log(vector[i]);
}

//forEach loop
sum = 0;
vectorNumeric.forEach(element => {
    sum += element;
});
console.log(sum);

//map
sum = 0;
vectorNumeric.map(element => sum+= element);
console.log(sum);

//5. Split
let propozitie = "Imi place sa codez si sa intru in apa marii";
let cuvinte = propozitie.split(" ");
console.log(cuvinte);

//6. JSON
let jsonObj = {
    nume: "",
    prenume: "",
    varsta: 0,
    proiecte: []
}

jsonObj.nume = "Ion";
jsonObj.prenume = "Popescu";
jsonObj.varsta = 20;
jsonObj.proiecte.push("SpringIT");
jsonObj.proiecte.push("TTJ");
jsonObj.proiecte.push("STS");

console.log(jsonObj);
//Parcurgere vector din JSON
jsonObj.proiecte.forEach(element => console.log(element));

//TEMA:
//1.Declarati un JSON numit cumparaturi cu proprietatile numeMagazin(string), listaCumparaturi vector(string), pretProduse vector(numeric)
//Asignati valorile variabilelor de mai jos in JSON-ul creat
//tip: variabila produse de tip string trebuie convertita intr-un vector de string-uri
let magazin = "Carrefour";
let produse = "Tigari,Bere,Fructe,Apa,Cola";
let preturi = [21.5, 3, 6, 2.5, 3.5];

//2. Creati o functie ce primeste ca parametru json-ul creat la exercitiul 1 si returneaza un vector de string unde fiecare element este de forma produs-pret
function returnArray(json) {
  
}

